﻿//Copyright(c) 2015 Sander Homan

//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files(the "Software"), to deal 
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject tothe following conditions:

//    The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.

//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.

var should = require('should');

describe('fnarg', function () {
    it('should return an empty array when given a function without arguments', function() {
        var fnarg = require('../');
        var result = fnarg(function () { });
        result.should.be.an.array;
        result.length.should.equal(0);
    });

    it('should return an array of arguments when given a function with arguments', function () {
        var fnarg = require('../');
        var result = fnarg(function (test, test2) { });
        result.should.be.an.array;
        result.length.should.equal(2);
        result[0].should.equal("test");
        result[1].should.equal("test2");
    });

    it('should throw an error when not given a function', function () {
        var fnarg = require('../');
        should.throws(function() {
             fnarg("test");
        });
    });
})