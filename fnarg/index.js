﻿//Copyright(c) 2015 Sander Homan

//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files(the "Software"), to deal 
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject tothe following conditions:

//    The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.

//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.

'use strict';

function index(fn) {
    if (typeof fn !== 'function')
        throw new Error("Invalid argument: fn is not a function");

    var functionString = fn.toString();
    var startIndex = functionString.indexOf('(');
    var endIndex = functionString.indexOf(')', startIndex+1);

    var argumentString = functionString.substring(startIndex + 1, endIndex).trim();
    if (argumentString === '')
        return [];
    

    var args = argumentString.split(',');
    for (var i = 0; i < args.length; i++) {
        args[i] = args[i].trim();
    }
    return args;
}

module.exports = index;